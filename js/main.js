const BASE_URL = "https://635f4b193e8f65f283b01221.mockapi.io/todos";

function getData() {
  loadingOn();
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then(function (res) {
      renderTable(res.data);
      loadingOff();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

getData();

function delTodo(idTodo) {
  loadingOn();
  axios({
    url: `${BASE_URL}/${idTodo}`,
    method: "DELETE",
  })
    .then(function (res) {
      getData();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function addTodo() {
  loadingOn();
  var newData = getInfoHTML();

  axios({
    url: BASE_URL,
    method: "POST",
    data: newData,
  })
    .then(function (res) {
      getData();
      resetForm();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

var currentID = null;
function changeTodo(idTodo) {
  loadingOn();
  axios({
    url: `${BASE_URL}/${idTodo}`,
    method: "GET",
  })
    .then(function (res) {
      currentID = res.data.id;
      document.getElementById("idTodo").innerHTML = `ID: ${res.data.id}`;
      document.getElementById("nameTodo").value = `${res.data.name}`;
      document.getElementById("descTodo").value = `${res.data.desc}`;
      document.getElementById(
        "completedTodo"
      ).value = `${res.data.isCompleted}`;
      loadingOff();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function updateTodo() {
  loadingOn();

  var newData = getInfoHTML();
  axios({
    url: `${BASE_URL}/${currentID}`,
    method: "PUT",
    data: newData,
  })
    .then(function (res) {
      getData();
      resetForm();
      currentID = null;
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
