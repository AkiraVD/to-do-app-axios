function renderTable(array) {
  var tableContent = "";

  array.forEach((data) => {
    tableContent += `
      <tr>
        <td>${data.id}</td>
        <td>${data.name}</td>
        <td>${data.desc}</td>
        <td><i class="material-icons fs-1 text-${
          data.isCompleted ? "success" : "danger"
        }">${data.isCompleted ? "check" : "clear"}</i></td>
        <td><button class="btn btn-danger mb-1" onclick="delTodo(${
          data.id
        })">Xóa</button>
        <a href="#header"><button class="btn btn-warning" onclick="changeTodo(${
          data.id
        })">Sửa</button></a></td>
      </tr>
      `;
  });
  document.getElementById("tbodyTodo").innerHTML = tableContent;
}

function getInfoHTML() {
  var name = document.getElementById("nameTodo").value;
  var desc = document.getElementById("descTodo").value;
  var isCompleted = document.getElementById("completedTodo").value === "true";

  return (data = {
    name: name,
    desc: desc,
    isCompleted: isCompleted,
  });
}

function resetForm() {
  document.getElementById("idTodo").innerHTML = `<br/>`;
  document.getElementById("nameTodo").value = "";
  document.getElementById("descTodo").value = "";
}

function loadingOn() {
  document.getElementById("loading").style.display = "flex";
}

function loadingOff() {
  document.getElementById("loading").style.display = "none";
}
